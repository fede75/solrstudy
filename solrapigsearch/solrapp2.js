

(function(){



var app = angular.module('solrapp2', []);
    angular.module('solrapp2')
      .directive('searchResults', function () {
        return {
            scope: {
                solrUrl: '@',
                displayField: '@',
                query: '@',
                results: '&'
            },
			
			
			
            restrict: 'E',
            controller: function ($scope, $http) {
                $scope.$watch('query', function () {
                    $http({
                        method: 'GET',
                        url: $scope.solrUrl,
                        params: {
                            'json.wrf': 'JSON_CALLBACK',
                            'q': $scope.query ,   //"*:*"
                            'fl': $scope.displayField,
							'wt':'json'
							

							
								
                        }
                    })
                        
						
						.success(function (data) {
                        var docs = data.objects;
						
                        $scope.results.docs = docs;
						console.log(data);
						

                    })
					
					.error(function (e) {console.log('Errore: '+e);});
                });
            },
			
            template: '<input ng-model="query" name="Search"></input>' +
                '<h2>Risultati della ricerca {{query}}</h2>' +
               
			     '<span ng-repeat="doc in results.docs">' +
                
				' <p>{{doc["PID"]}}</p>' +
				' <p>{{doc["uw.general.description"]}}</p>' +
				 
				'  <img src="https://fc.cab.unipd.it/fedora/objects/{{doc["PID"]}}/methods/bdef:Asset/getThumbnail"></img></p>' +
				
			    '</span>'

        };
    });
	
	
	
})();	