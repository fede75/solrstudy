var app = angular.module('solrapp', []);
    angular.module('solrapp')
      .directive('searchResults', function () {
        return {
            scope: {
                solrUrl: '@',
                displayField: '@',
                query: '@',
                results: '&'
            },
			
			
			
            restrict: 'E',
            controller: function ($scope, $http) {
                $scope.$watch('query', function () {
					
					
										
					
                    $http({
                        method: 'GET',
                        url: $scope.solrUrl,
                        params: {
                            //'json.wrf': 'JSON_CALLBACK',
                            'q': $scope.query /*'*:*'*/,
                            'fl': $scope.displayField,
							'wt':'json',
							'fq':'ispartof:"'+$scope.myFq+'"', 
							'qf':'pid',
							//'facet':'true',
                            //'defType':'edismax',
							'rows':'50',
						
							//'start':'2',
							
							
                        }
                    })
                        
						
						.success(function (data) {
                        var docs = data.response;
						
                        $scope.results.docs = data.response.docs;
						console.log(data.response.docs);
						
						

                    })
					
					.error(function (e) {console.log('Errore: '+e);});
                });
            },
            template: '<input ng-model="query" name="Search"></input>' +
			    'FQ:<select ng-model="myFq"> ' +
				'	<option value="o:87">87</option> '+
				'	<option value="o:75">75</option> '+
				'	<option value="o:132">132</option> '+
				'</select>'+				
                '<h2>Risultati della ricerca {{query}}</h2>' +
               
			   '<span ng-repeat="doc in results.docs">' +
                '<table>'+
				'<td>'+
				'  <p>{{doc["pid"]}}</p>' +
				'  <p>{{doc["owner"]}}</p>' +
				'  <p>{{doc["dc_description"]}}</p>' +
				'</td>'+
				'<td>'+
				'<p><img src=\'https://phaidradev.cab.unipd.it/preview/{{doc["pid"]}}/ImageManipulator/boxImage/480/png\'</p>'+
				'</td>'+
				'</table>'+
				'<hr></hr>'
				
			    
        };
    });
	